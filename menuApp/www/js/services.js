angular.module("mcHackServices", [])

.factory("RegistractionService", function($http, $window, $q, mcHackAjaxConfig) {
    function registerRequest(username, email, password, checked) {
        var deferred = $q.defer();

        // user already logged in
        if ($window.localStorage["username"] === username &&
            $window.localStorage["email"] === email &&
            $window.localStorage["password"] === password) {

            console.info("same credentials!");

            // session key exist, return existing key rather than fetch again
            if ($window.localStorage["session_key"]) {
                deferred.resolve($window.session_key);

                return deferred.promise;
            }
        }

        $window.localStorage["username"] = username;
        $window.localStorage["email"] = email;
        $window.localStorage["password"] = password;

        return $http({
                method: "POST",
                url: AjaxConfig.baseUrl + "register.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    uname: username,
                    email: email,
                    pw: password,
                    checked: checked
                }
            })
            .then(function(res) {
                console.info("congrat! uname: " + username + ", email:" + email + ", pw:" + password + ", checked: " + checked);

                var sessionKey = res.data.session_key;

                if (sessionKey) {
                    $window.localStorage["session_key"] = sessionKey;

                    return sessionKey;
                } else {

                    throw "sessionKey is missing";
                }
            }, function(reason) {
                console.warn("cannot register! uname: " + username + ", email:" + email + ", pw:" + password + ", checked: " + checked);

                throw reason;
            });
    }

    return {
        registerRequest: registerRequest
    };
})

.factory("OrderService", function($http, $window, $q, mcHackAjaxConfig) {
    function orderRequest(order) {
        var deferred = $q.defer(),
            sessionKey;

        // session key is missing
        if (!$window.localStorage["session_key"]) {
            deferred.reject("session key is not found.");

            return deferred.promise;
        }

        // existing order
        if ($cookies.order_code) {
            console.warn("an order is placed and in queue");
            deferred.reject($cookies.order_code + " is already placed");

            return deferred.promise;
        }

        sessionKey = $window.localStorage["session_key"];

        return $http({
                method: "POST",
                url: AjaxConfig.baseUrl + "order.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    sessionKey: sessionKey,
                    order: order
                }
            })
            .then(function(res) {
                var orderCode = res.data.order_code;

                if (orderCode) {
                    $window.localStorage["order_code"] = orderCode;
                    console.info("congrats! order is placed, order code: " + orderCode);

                    return orderCode;
                } else {

                    throw "order code is missing";
                }
            });
    }

    return {
        orderRequest: orderRequest
    };
});