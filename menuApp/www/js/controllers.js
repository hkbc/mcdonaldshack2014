angular.module('mcHackControllers', ['ngCordova'])


.controller('StartCtrl', function($scope, $state) {
  // ionic.Platform.ready(function() {
  //   // hide the status bar using the StatusBar plugin
  //   $cordovaStatusbar.hide();
  // });
  $scope.startApp = function() {
    console.log("startApp");
    $state.go("app.order");
  }
})

.controller('MakeMealCtrl', function($scope, $state) {
  console.log("make your meal!");
})


.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller("OrderCtrl", function($scope, $ionicPopup, OrderService, $cordovaPush, $ionicPlatform, $cordovaDialogs) {
  $scope.defaultActiveSlide = 1;

  var lock = false,
    setLock = function() {
      lock = true;
    },
    releaseLock = function() {
      lock = false;
    };

  $scope.placeOrder = function(meal) {

    if (lock) return;

    setLock();

    // dummy order now
    var meal = meal || {
      title: "Happy Meal",
      id: "dfjklsf"
    };

    if (!meal) {
      releaseLock();
      return console.error("order is missing");
    }


    var confirmPopup = $ionicPopup.confirm({
      title: "Confirm order?",
      template: 'Are you sure you want to confirm ordering ' + meal.title + '?'
    });

    return confirmPopup.then(function(res) {
      if (res) {

        return OrderService.orderRequest(meal)
          .then(function(data) {
              releaseLock();
              console.info("congrat! meal: " + meal);
              console.info("meal key: " + data);
            },
            function(reason) {
              releaseLock();
              console.warn("cannot register! meal: " + meal + ", reason: " + reason);
            });
      } else {
        releaseLock();
        throw 'Order cancelled';
      }
    });

  };
  var androidConfig = {
    "senderID": "116691054888",
    "ecb": "onNotification"
  };

  console.log("before");
  $ionicPlatform.ready(function() {
    console.log("step1");
    $cordovaPush.register(androidConfig).then(function(result) {
      // Success!
      console.log("step Success");
      $scope.pushSuccess = result;
    }, function(err) {
      console.log("step Err");
      $scope.pushSuccess = err;
    });



  });
  // receive notification

  onNotification = function(e) {

    switch (e.event) {
      case 'registered':
        if (e.regid.length > 0) {
          console.log("Your regID is : " + e.regid);
        }
        break;

      case 'message':
        // this is the actual push notification. its format depends on the data model     from the push server
        //alert(e.payload.message);
        $cordovaDialogs.confirm(e.payload.message, 'Mc Way', ['Confirm', 'Cancel'])
          .then(function(buttonIndex) {
            // no button = 0, 'OK' = 1, 'Cancel' = 2
            var btnIndex = buttonIndex;
          });

        angular.element(document.querySelector('#yata')).html(e.message);

        break;

      case 'error':
        console.log('GCM error = ' + e.msg);
        break;

      default:
        console.log('An unknown GCM event has occurred');
        break;
    }
  };

})

.controller("OrderSlideBox1Ctrl", function($scope) {})

.controller("OrderSlideBox4Ctrl", function($scope) {})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [{
    title: 'Reggae',
    id: 1
  }, {
    title: 'Chill',
    id: 2
  }, {
    title: 'Dubstep',
    id: 3
  }, {
    title: 'Indie',
    id: 4
  }, {
    title: 'Rap',
    id: 5
  }, {
    title: 'Cowbell',
    id: 6
  }];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {});