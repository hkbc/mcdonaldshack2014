var mcHackControllers = angular.module("mcHackControllers", []);

mcHackControllers.controller("StartCtrl", ["$scope", "$http",
  function($scope, $http) {
    // $http.get("phones/phones.json").success(function(data) {
    //   $scope.phones = data;
    // });

    // $scope.orderProp = "age";
  }
]);

mcHackControllers.controller("RegistrationCtrl", ["$scope", "RegistractionService",
  function($scope, RegService) {
    $scope.placeRegister = function() {

      var uname = $scope.uname,
          email = $scope.email,
          pw = $scope.pw,
          checked = $scope.checked;


      if (!uname || !email || !pw) {

        return console.error("fields are missing");
      }

      return RegService.registerRequest(uname, email, pw, checked)
        .then(function(data) {
            console.info("congrat! uname: " + uname + ", email:" + email + ", pw:" + pw + ", checked: " + checked);
            console.info("session key: " + data);
          },
          function(reason) {
            console.warn("cannot register! uname: " + uname + ", email:" + email + ", pw:" + pw + ", checked: " + checked + ", reason: " + reason);
          });
    }
  }
]);

mcHackControllers.controller("OrderCtrl", ["$scope", "OrderService",
  function($scope, OrderService) {
    $scope.placeOrder = function() {

      // dummy order now
      var order = {status: "dummy"};

      if (!order) {

        return console.error("order is missing");
      }

      return OrderService.orderRequest(order)
        .then(function(data) {
            console.info("congrat! order: " + order);
            console.info("order key: " + data);
          },
          function(reason) {
            console.warn("cannot register! order: " + order + ", reason: " + reason);
          });
    }
  }
]);

mcHackControllers.controller("LeaderboardCtrl", ["$scope", "$routeParams",
  function($scope, $routeParams) {
    // $scope.phoneId = $routeParams.phoneId;
  }
]);