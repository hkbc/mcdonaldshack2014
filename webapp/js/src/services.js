var mcHackServices = angular.module("mcHackServices", ["ngCookies"]);

mcHackServices.factory("RegistractionService", ["$http", "$cookies", "$q", "mcHackAjaxConfig", function($http, $cookies, $q, AjaxConfig) {
    function registerRequest(username, email, password, checked) {
        var deferred = $q.defer();

        // user already logged in
        if ($cookies.username === username &&
            $cookies.email === email &&
            $cookies.password === password) {

            console.info("same credentials!");

            // session key exist, return existing key rather than fetch again
            if ($cookies.session_key) {
                deferred.resolve($cookies.session_key);

                return deferred.promise;
            }
        }

        $cookies.username = username;
        $cookies.email = email;
        $cookies.password = password;

        return $http({
                method: "POST",
                url: AjaxConfig.baseUrl + "register.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    uname: username,
                    email: email,
                    pw: password,
                    checked: checked
                }
            })
            .then(function(res) {
                console.info("congrat! uname: " + username + ", email:" + email + ", pw:" + password + ", checked: " + checked);

                var sessionKey = res.data.session_key;

                if (sessionKey) {
                    $cookies.session_key = sessionKey;

                    return sessionKey;
                } else {

                    throw "sessionKey is missing";
                }
            }, function(reason) {
                console.warn("cannot register! uname: " + username + ", email:" + email + ", pw:" + password + ", checked: " + checked);

                throw reason;
            });
    }

    return {
        registerRequest: registerRequest
    };
}]);

mcHackServices.factory("OrderService", ["$http", "$cookies", "$q", "$location", "mcHackAjaxConfig", function($http, $cookies, $q, $location, AjaxConfig) {
    function orderRequest(order) {
        var deferred = $q.defer(),
            sessionKey;

        // session key is missing
        if (!$cookies.session_key) {
            $location.url("/registration");
            deferred.reject("session key is not found. Redirect to registration page");

            return deferred.promise;
        }

        // existing order
        if ($cookies.order_code) {
            console.warn("an order is placed and in queue");
            deferred.reject($cookies.order_code + " is already placed");

            return deferred.promise;
        }

        sessionKey = $cookies.session_key;

        return $http({
                method: "POST",
                url: AjaxConfig.baseUrl + "order.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    sessionKey: sessionKey,
                    order: order
                }
            })
            .then(function(res) {
                var orderCode = res.data.order_code;

                if (orderCode) {
                    $cookies.order_code = orderCode;
                    console.info("congrats! order is placed, order code: " + orderCode);

                    return orderCode;
                } else {

                    throw "order code is missing";
                }
            });
    }

    return {
        orderRequest: orderRequest
    };
}]);