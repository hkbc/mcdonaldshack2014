var mcHackApp = angular.module("mcHackApp", [
    "ngRoute",
    "mcHackControllers",
    "mcHackServices",
    "mcHackConfig"
    ]);

mcHackApp.config(["$routeProvider",
  function($routeProvider) {
    $routeProvider.
    when("/start", {
        templateUrl: "partials/start.html",
        controller: "StartCtrl"
    }).
    when("/registration", {
        templateUrl: "partials/registration.html",
        controller: "RegistrationCtrl"
    }).
    when("/order", {
        templateUrl: "partials/order.html",
        controller: "OrderCtrl"
    }).
    when("/leaderboard", {
        templateUrl: "partials/leaderboard.html",
        controller: "LeaderboardCtrl"
    }).
    otherwise({
        redirectTo: "/start"
    });
}]);