McHack Webapp
=============

### Prerequisite
1. git (version control)
2. nodejs
    - npm
        - grunt-cli
3. ruby gem
    - sass      (3.4.7)
    - compass   (1.0.1 in order to work with sass source map)

### Step to setup development environment
1. ```git clone REPO```
2. ```npm install``` (all grunt tasks package needed are setup in package.json)
3. ```bower install``` (all bower js library package needed are setup in bower.json)
4. Init Project: ```grunt build``` __vs__ Development: ```grunt``` __vs__ Depolyment: ```grunt depoly```  
5. Start coding!

### Tools detail
#### 1. sass
- __sass structure__  
index.scss  
┖partials/_variables - save all variables  
┖partials/_mixins - common css for views  
┖partials/_typography - font type, size, etc  
┖views/* - css for different views

#### 2. grunt
- __grunt tasks__
    + #####Development tasks (grunt)
        1. watch
            - live reload __html__, __php__, __images__
            - compass watch and complie __scss__ to __css__
            - live __jshint__ checking 
            
    + #####Deployment tasks (grunt depoly)   
        1. compass
            - config setting: ```config.rb```
        2. jshint
            - config setting: ```.jshintrc```
        3. uglify
            - config setting: ```Gruntfile.js -> uglify```
