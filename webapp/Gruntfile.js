module.exports = function(grunt) {

  // load all grunt tasks
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    // watch for changes and trigger compass, jshint, uglify and livereload
    watch: {
      options: {
        livereload: true,
      },
      compass: {
        files: ["scss/**/*.{scss,sass}"],
        tasks: ["compass"]
      },
      js: {
        files: "<%= jshint.all %>",
        tasks: [
          "jshint" //,
          // "uglify"
          ]
        },
        livereload: {
          files: ["*.html", "*.php", "images/**/*.{png,jpg,jpeg,gif,webp,svg}"]
        }
      },

    // compass and scss
    compass: {
      dist: {
        options: {
          config: "config.rb",
          sourcemap: true
        }
      }
    },

    // javascript linting with jshint
    jshint: {
      options: {
        jshintrc: ".jshintrc",
        "force": true
      },
      all: [
        // "Gruntfile.js",
        "js/**/*.js"
        ]
      },

    // uglify to concat, minify, and make source maps
    uglify: {
      dist: {
        options: {
          banner: "/*! <%= pkg.name %> <%= grunt.template.today('yyyy-mm-dd') %> */\n"
        },
        files: {
          "js/plugins.min.js": [
          "bower_components/**/*.js",
          "js/plugins/**/*.js"
          ],
          "js/src.min.js": [
          "js/src/**/*.js"
          ]
        }
      }
    }
  });

  // Default task(s).
  grunt.registerTask("default", ["watch"]);
  grunt.registerTask("build", ["jshint", "compass"]);
  grunt.registerTask("depoly", ["jshint", "uglify", "compass"]);

};
