McDonalds Hackathon 2014
========================

###Concepts
1. Pre-order in mobile / webapp and pickup at store
    - choose the order in app
    - pay by paypal etc once it is confirmed at store with e.g. __oyster-card-like contactless solution (iBeacon, NFC?)__, or credit card (similar to cinemas in Hong Kong)
    - order confirm and store will preceed the order, wait for picking up (app will notify the user)
    
2. Custom Meal Leaderboard
    - when choosing custom combination, calories and price would be shown
    - search from history
    - leaderboard for custom combination will be shown, i.e. you can pickup the most popular meal in this month!
    - certain catagories can be applied, e.g. beefless, <1000 cal, UK best seller

###Setup
1. App (mobile, web) - __(UA: User App)__
    - prepare custom order
    - show leaderboard
    - save / confirm order
    - pickup notice
    - stores locator (optional)

2. Receiver - __(RA: Shop Receiver App)__
    - to confirm user is at the store
    - place order
    - show history
    
3. Identifier (iBeacon, Mobile?) - __(iBeacon itself, no app / service is associated)__
    - for Receiver to identify the unique identity
    - has to be linked with mobile app
    
4. Server - __(BS: Backend Server)__
    - receive order
    - retrive and update leaderboard items
    - put orders into queue for store to proceed
    - store crew to place ready to pickup request
    - notify user to pickup order

###Application needed
1. Register page
    - __(UA-1)__: register user information (name, email, card details) and pair up iBeacon 
    - __(UA-2)__: POST registration form to __BS__ and update __UA__ UI when success __(==> BS-1)__

2. Order page
    - __(UA-3)__: choose order 
    - __(UA-4)__: POST order to __BS__ and update __UA__ UI when success (reminder is needed for confirm at shop) __(==> BS-2)__
    - __(UA-7)__: receive notification when order is ready to pickup __(==> BS-6)__

3. Leaderboard page
    - __(UA-5)__: GET leaderboard data from __BS__ and update __UA__ UI  __(==> BS-4)__
    - __(UA-6)__: choose leaderboard item will switch to order page, i.e. __UA-4__ 

4. Receiver app
    - __(RA-1)__: take iBeacon ID 
    - __(RA-2)__: GET user details and orders __(==> BS-3)__
    - __(RA-3)__: POST confirmation to server __(==> BS-5)__  

5. Server
    - __(BS-1)__: receive registration from __UA (==> UA-2)__ and update DB
    - __(BS-2)__: receive order from __UA (==> UA-4)__ and update DB
    - __(BS-3)__: deliver user details and orders to __RA (==> RA-2)__
    - __(BS-4)__: deliver leaderboard to __UA (==> UA-5)__
    - __(BS-5)__: receive order confirmation from __RA (==> RA-3)__
    - __(BS-6)__: send notification for picking up to __UA (==> UA-7)__

###Technical Specification
1. Webapp
    - angularjs: ready to use MVC framework for single page application

2. Mobile app and Receiver app
    - native, as we have to support:
    i. iBeacon APIs
    ii. notification

3. Server
    - PHP as it is most stable, most knowledgable at the moment (for HKBC)
    - follow [RESTful API level 3](http://martinfowler.com/articles/richardsonMaturityModel.html)
    - JSON format response
