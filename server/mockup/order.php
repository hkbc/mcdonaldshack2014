<?php

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

$session_key = $obj['sessionKey'];
$order = $obj['order'];

if (!isset($session_key)) {
    $result = array('error_message' => 'session key missing');
} else if (!isset($order)) {
    $result = array('error_message' => 'order is missing');
} else {
    $result = array('order_code' => md5(implode(array_values($order))));
}

echo json_encode($result);


/*
 * Push Notification, you would need to pass th
 */
function android_notification($regId="APA91bFmtMPawEQDC5jo2FrwdBSWa3iljtwYUmt0YXGA51b32IVH0t52xlSyav7gv-Eug4Cs-7ytIQeuT50ZjSXbDElIAxOXXAUbOzzRx5AECj_cqpwSUHtX7gDkYJHtXc-aaH3yOLIg4E--5o_SsrPuhygvpz5tqnomeV02GeWqG-mZOKB4tjE", $title ="", $msg=""){

    define('API_ACCESS_KEY', 'AIzaSyBeKMkOa6WLVL2MD7ON2bNgePqAm7JMkRs');

    $registrationIds = array($regId);

    // prep the bundle
    $msg = array
    (
        'message' =>$msg,
        'title' => $title,
        'subtitle' => "",
        'tickerText' => "",
        'vibrate' => 1,
        'sound' => 1,
        'largeIcon' => 'large_icon',
        'smallIcon' => 'small_icon'
    );

    $fields = array
    (
        'registration_ids' => $registrationIds,
        'data' => $msg
    );

    $headers = array
    (
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if (curl_errno($ch)) {
        $result = 'error:' . curl_error($ch);
    }

    curl_close($ch);


    echo $result;
}