<?php

$json = file_get_contents('php://input');
$obj = json_decode($json);


$uname = $obj->uname;
$email = $obj->email;
$pw = $obj->pw;

if (!isset($uname) || !isset($email) || !isset($pw)) {
    $result = array('error_message' => 'parameter missing');
} else {
    $result = array('session_key' => md5($uname . $email . $pw));
}

echo json_encode($result);