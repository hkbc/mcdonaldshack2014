<?php

class Order_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function get_all_orders(){
		$orders = $this->db->get('orders')->result_array();
		$this->append_order_details($orders);
		return $orders;
	}

	public function get_my_orders_all($user_id){
		$orders = $this->db->get_where('orders',array('user_id' => $user_id))->result_array();
		$this->append_order_details($orders);
		return $orders;
	}

	/**
	 * Available Status:
	 *	1. new
	 *	2. paid
	 *	3. collected
	 *	4. cancelled
	 */
	public function get_my_orders_by_status($user_id, $status){
		$orders = $this->db->get_where('orders',array('user_id' => $user_id, 'status' => $status))->result_array();
		$this->append_order_details($orders);
		return $orders;
	}

	public function add_new_order($user_id, $food_ids){
		
	
	}

	public function add_new_order_by_meal_id(){
	
	}

	/**
	 * Order can only be cancelled when it's new order
	 */
	public function cancel_order($order_id){
	
	}

	/**
	 * Order details
	 */
	private function append_order_details( &$orders ){
		if($orders !== NULL){
			foreach($orders as &$order){
				$order['details'] = $this->get_order_details($order['id']);
			}
		}
	}

	 private function get_order_details($order_id){
		 $sql = 'SELECT f.id, f.name, f.price, f.cal, f.photo, f.type
				 FROM order_details od 
				 LEFT JOIN food f ON od.food_id = f.id
				 WHERE od.order_id = ?';
		return $this->db->query($sql, $order_id)->result_array;
	 }
