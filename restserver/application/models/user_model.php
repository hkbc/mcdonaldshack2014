<?php

class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function login($user, $pw) {
		//check if user exist
		if($this->db->get_where('users', array('username' => $user, 'pw' => $pw))->row()){
			//generate a session_key
			$key = md5(microtime().rand());
			$this->db->query('UPDATE users SET session_id = ? WHERE username = ?', array($key, $user));
			return $key; 
		} else {
			return false;
		}
	}

	public function is_logged_in($key){
		if($key){
			$user = $this->db->get_where('users', array('session_id' => $key))->row();
		}

		if($user !== NULL){
			return $user_id;
		}
		else
		{
			return false;
		}
	}

	public function set_beacon(){
		$this->db->query('UPDATE users SET status = "0" WHERE id = "2"');
	}

	public function get_beacon_status(){
		$row = $this->db->query('SELECT status FROM users WHERE id = "2"')->row_array();
		return $row['status'];
	}

	public function reset_beacon(){
		$this->db->query('UPDATE users SET status = "1"');
	}

}
