<?php

class Meal_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function get_top($limit = NULL){
		$sql = 'SELECT m.freq, 
						f1.name AS food1_name, 
						f2.name AS food2_name, 
						f3.name AS food3_name, 
						f4.name AS food4_name,
						f1.id AS food1_id,
						f2.id AS food2_id,
						f3.id AS food3_id,
						f4.id AS food4_id
				FROM meals m
				LEFT JOIN food f1
					ON m.food1_id = f1.id AND m.food1_id IS NOT NULL AND m.food1_id !="0" AND m.food1_id !=""
				LEFT JOIN food f2
					ON m.food2_id = f2.id AND m.food2_id IS NOT NULL AND m.food2_id !="0" AND m.food2_id !=""
				LEFT JOIN food f3
					ON m.food3_id = f3.id AND m.food3_id IS NOT NULL AND m.food3_id !="0" AND m.food3_id !=""
				LEFT JOIN food f4
					ON m.food4_id = f4.id AND m.food4_id IS NOT NULL AND m.food4_id !="0" AND m.food4_id !=""
				GROUP BY m.id
				ORDER BY freq DESC ';
		if(is_numeric($limit) && $limit !== 0){
			$sql .= ' LIMIT ' . $limit;
		}
		return $this->db->query($sql)->result_array();
	}

	public function get_food_type($type){
		return $this->db->get_where('food', array('type' => $type))->result_array();
	}
	
}
