<?php

class Food_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function get_food_id($id){
		if(is_numeric($id))
			return $this->db->get_where('food', array('id' => $id))->result_array;
	}

	public function get_all_food(){
		return $this->db->get('food')->result_array();
	}

	public function get_food_type($type){
		return $this->db->get_where('food', array('type' => $type))->result_array();
	}

	
}
