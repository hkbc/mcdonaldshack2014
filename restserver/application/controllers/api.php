<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class api extends REST_Controller
{
	public function __construct(){
		parent::__construct();  
	}

	/**
	 * LOGIN 
	 */
	public function login_post(){
		$this->load->model('User_model', 'muser');
		if($this->post('username') && $this->post('password')){
			$key = $this->muser->login($this->post('username'), $this->post('password'));
		} else  {
			$key = false;
		}

		if($key){
			$this->response(array('success' => true, 'session_id' => $key));
		} else {
			$this->response(array('success' => false, 'session_id' => 0));
		}
	}

	/**
	 * GET stuff (LOGIN NOT REQUIRED)
	 */
	public function food_get(){
		$this->load->model('Food_model', 'mfood');
		if($this->get('type')){
			$this->response($this->mfood->get_food_type($this->get('type')));	
		} else if($this->get('id')) {
		} else {
			$this->response($this->mfood->get_all_food());
		}
	}

	public function meals_get(){
		$this->load->model('Meal_model', 'mmeal');
		if($this->get('limit')){
			$this->response($this->mmeal->get_top());
		} else {
			$this->response($this->mmeal->get_top());
		}
	} 

	public function get_orders_post(){
			
	}

	/**
	 * ibeacon
	 */
	public function ibeacon_get(){
		if($this->get('uuid')){
			$this->load->model('User_model', 'muser');
			echo $this->muser->get_beacon_status();
			if($this->muser->get_beacon_status() === '1'){
				$this->muser->set_beacon();
				$this->android_notification();
				echo 1;
				return 1;
			}
		}
		echo 0;
		return 0;
	}

	public function ibeacon_reset_get(){
		$this->load->model('User_model', 'muser');
		$this->muser->reset_beacon();
		return 1;
	}
	/**
	 * Push Notification
	 */
	public function android_notification($regId="APA91bFmtMPawEQDC5jo2FrwdBSWa3iljtwYUmt0YXGA51b32IVH0t52xlSyav7gv-Eug4Cs-7ytIQeuT50ZjSXbDElIAxOXXAUbOzzRx5AECj_cqpwSUHtX7gDkYJHtXc-aaH3yOLIg4E--5o_SsrPuhygvpz5tqnomeV02GeWqG-mZOKB4tjE", $title ="", $msg=""){

		define('API_ACCESS_KEY', 'AIzaSyBeKMkOa6WLVL2MD7ON2bNgePqAm7JMkRs');

		$registrationIds = array($regId);
		$order_details = array(
							'food1' => 'Apple Pie',
							'food2' => 'Strawberry Sundae',
							'price' => '2.89' 
							);

		$order_msg = "Order: \n1x Apple Pie 1.39\n1x Strawberry Sundae 1.19\n\nTotal: 2.58\n\nConfirm your order? ";
		// prep the bundle
		$msg = array
			(
				'message' => $order_msg,
				'title' => $title,
				'subtitle' => "",
				'tickerText' => "",
				'vibrate' => 1,
				'sound' => 1,
				'largeIcon' => 'large_icon',
				'smallIcon' => 'small_icon',
				'order_details' => $order_details
			);

		$fields = array
			(
				'registration_ids' => $registrationIds,
				'data' => $msg
			);

		$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			$result = 'error:' . curl_error($ch);
		}

		curl_close($ch);

		echo $result;
	}

	/**
	 * Ordering
	 * Structure: 
	 *
	 *
	 */
	public function newOrder_post(){

		$this->load->model('Meal_model');
	
	}

	/**
	 * check if logged in
	 */
	private function is_logged_in($key){
		$this->load->model('User_model', 'muser');
		return $this->muser->is_logged_in($key);
	}

}
